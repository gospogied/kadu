<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="es">
<context>
    <name>@default</name>
    <message>
        <source>Hints</source>
        <translation>Consejos</translation>
    </message>
    <message>
        <source>Look</source>
        <translation>Apariencia</translation>
    </message>
    <message>
        <source>Notifications</source>
        <translation>Notificaciones</translation>
    </message>
    <message>
        <source>Layout</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>New hints go</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Auto</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Own hints position</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>X</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Y</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Minimum width</source>
        <translation>Anchura mínima</translation>
    </message>
    <message>
        <source>Maximum width</source>
        <translation>Anchura máxima</translation>
    </message>
    <message>
        <source>Hints position preview</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Timeout</source>
        <translation>Tiempo de espera</translation>
    </message>
    <message>
        <source>Font color</source>
        <translation>Color de fuente</translation>
    </message>
    <message>
        <source>Background color</source>
        <translation>Color de fondo</translation>
    </message>
    <message>
        <source>Font</source>
        <translation>Fuente</translation>
    </message>
    <message>
        <source>Left button</source>
        <translation>Botón izquierdo</translation>
    </message>
    <message>
        <source>Nothing</source>
        <translation>Nada</translation>
    </message>
    <message>
        <source>Middle button</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Right button</source>
        <translation>Botón derecho</translation>
    </message>
    <message>
        <source>&apos;Open chat&apos; works on all events</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Show message content in hint</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Number of quoted characters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Delete pending message when user deletes hint</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Buddy List</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Hint Over Buddy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Border color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Border width</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Transparency</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Mouse Buttons</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Syntax</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Icon size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>16px</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>22px</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>32px</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>48px</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>64px</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>New Chat/Message</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Advanced</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source> px</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%</source>
        <translation type="unfinished"></translation>
    </message>
    <message numerus="yes">
        <source>%n second(s)</source>
        <translation type="unfinished">
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message>
        <source>Margin size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Hint&apos;s corner</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>On Top</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>On Bottom</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Top Left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Top Right</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Bottom Left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Bottom Right</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Open Chat</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Delete Hint</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Delete All Hints</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;b&gt;Here&lt;/b&gt; you can see the preview</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Show buttons only if notification requires user&apos;s action</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Hints size and position...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>HintManager</name>
    <message>
        <source>&lt;table&gt;&lt;tr&gt;&lt;td align=&quot;left&quot; valign=&quot;top&quot;&gt;&lt;img style=&quot;max-width:64px; max-height:64px;&quot; src=&quot;{#{avatarPath} #{avatarPath}}{~#{avatarPath} @{kadu_icons/kadu:64x64}}&quot;&gt;&lt;/td&gt;&lt;td width=&quot;100%&quot;&gt;&lt;div&gt;[&lt;b&gt;%a&lt;/b&gt;][&amp;nbsp;&lt;b&gt;(%g)&lt;/b&gt;]&lt;/div&gt;[&lt;div&gt;&lt;img height=&quot;16&quot; width=&quot;16&quot; src=&quot;#{statusIconPath}&quot;&gt;&amp;nbsp;&amp;nbsp;%u&lt;/div&gt;][&lt;div&gt;&lt;img height=&quot;16&quot; width=&quot;16&quot; src=&quot;@{phone:16x16}&quot;&gt;&amp;nbsp;&amp;nbsp;%m&lt;/div&gt;][&lt;div&gt;&lt;img height=&quot;16&quot; width=&quot;16&quot; src=&quot;@{mail-message-new:16x16}&quot;&gt;&amp;nbsp;&amp;nbsp;%e&lt;/div&gt;]&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;[&lt;hr&gt;&lt;b&gt;%s&lt;/b&gt;][&lt;b&gt;:&lt;/b&gt;&lt;br&gt;&lt;small&gt;%d&lt;/small&gt;]</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>HintOverUserConfigurationWindow</name>
    <message>
        <source>Hint Over Buddy Configuration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Update preview</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Syntax</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>HintsConfigurationUiHandler</name>
    <message>
        <source>Configure</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Hint over buddy list: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Advanced hints&apos; configuration</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>HintsConfigurationWidget</name>
    <message>
        <source>Configure</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;b&gt;Here&lt;/b&gt; you can see the preview</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>HintsConfigurationWindow</name>
    <message>
        <source>Hints configuration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Don&apos;t hide</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;b&gt;Here&lt;/b&gt; you can see the preview</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
