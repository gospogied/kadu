<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en">
<context>
    <name>@default</name>
    <message>
        <source>Hints</source>
        <translation>Hints</translation>
    </message>
    <message>
        <source>Look</source>
        <translation>Look</translation>
    </message>
    <message>
        <source>Notifications</source>
        <translation>Notifications</translation>
    </message>
    <message>
        <source>Layout</source>
        <translation>Layout</translation>
    </message>
    <message>
        <source>New hints go</source>
        <translation>New hints go</translation>
    </message>
    <message>
        <source>Auto</source>
        <translation>Auto</translation>
    </message>
    <message>
        <source>Own hints position</source>
        <translation>Own hints position</translation>
    </message>
    <message>
        <source>X</source>
        <translation>X</translation>
    </message>
    <message>
        <source>Y</source>
        <translation>Y</translation>
    </message>
    <message>
        <source>Minimum width</source>
        <translation>Minimum width</translation>
    </message>
    <message>
        <source>Maximum width</source>
        <translation>Maximum width</translation>
    </message>
    <message>
        <source>Hints position preview</source>
        <translation>Hints position preview</translation>
    </message>
    <message>
        <source>Timeout</source>
        <translation>Timeout</translation>
    </message>
    <message>
        <source>Font color</source>
        <translation>Font color</translation>
    </message>
    <message>
        <source>Background color</source>
        <translation>Background color</translation>
    </message>
    <message>
        <source>Font</source>
        <translation>Font</translation>
    </message>
    <message>
        <source>Left button</source>
        <translation>Left button</translation>
    </message>
    <message>
        <source>Nothing</source>
        <translation>Nothing</translation>
    </message>
    <message>
        <source>Middle button</source>
        <translation>Middle button</translation>
    </message>
    <message>
        <source>Right button</source>
        <translation>Right button</translation>
    </message>
    <message>
        <source>&apos;Open chat&apos; works on all events</source>
        <translation>&apos;Open chat&apos; works on all events</translation>
    </message>
    <message>
        <source>Show message content in hint</source>
        <translation>Show message content in hint</translation>
    </message>
    <message>
        <source>Number of quoted characters</source>
        <translation>Number of quoted characters</translation>
    </message>
    <message>
        <source>Delete pending message when user deletes hint</source>
        <translation>Delete pending message when user deletes hint</translation>
    </message>
    <message>
        <source>Buddy List</source>
        <translation>Buddy List</translation>
    </message>
    <message>
        <source>Hint Over Buddy</source>
        <translation>Hint Over Buddy</translation>
    </message>
    <message>
        <source>Border color</source>
        <translation>Border color</translation>
    </message>
    <message>
        <source>Border width</source>
        <translation>Border width</translation>
    </message>
    <message>
        <source>Transparency</source>
        <translation>Transparency</translation>
    </message>
    <message>
        <source>Mouse Buttons</source>
        <translation>Mouse Buttons</translation>
    </message>
    <message>
        <source>Syntax</source>
        <translation>Syntax</translation>
    </message>
    <message>
        <source>Icon size</source>
        <translation>Icon size</translation>
    </message>
    <message>
        <source>16px</source>
        <translation>16px</translation>
    </message>
    <message>
        <source>22px</source>
        <translation>22px</translation>
    </message>
    <message>
        <source>32px</source>
        <translation>32px</translation>
    </message>
    <message>
        <source>48px</source>
        <translation>48px</translation>
    </message>
    <message>
        <source>64px</source>
        <translation>64px</translation>
    </message>
    <message>
        <source>New Chat/Message</source>
        <translation>New Chat/Message</translation>
    </message>
    <message>
        <source>Advanced</source>
        <translation>Advanced</translation>
    </message>
    <message>
        <source> px</source>
        <translation> px</translation>
    </message>
    <message>
        <source>%</source>
        <translation>%</translation>
    </message>
    <message numerus="yes">
        <source>%n second(s)</source>
        <translation>
            <numerusform>%n second</numerusform>
            <numerusform>%n seconds</numerusform>
        </translation>
    </message>
    <message>
        <source>Margin size</source>
        <translation>Margin size</translation>
    </message>
    <message>
        <source>Hint&apos;s corner</source>
        <translation>Hint&apos;s corner</translation>
    </message>
    <message>
        <source>On Top</source>
        <translation>On Top</translation>
    </message>
    <message>
        <source>On Bottom</source>
        <translation>On Bottom</translation>
    </message>
    <message>
        <source>Top Left</source>
        <translation>Top Left</translation>
    </message>
    <message>
        <source>Top Right</source>
        <translation>Top Right</translation>
    </message>
    <message>
        <source>Bottom Left</source>
        <translation>Bottom Left</translation>
    </message>
    <message>
        <source>Bottom Right</source>
        <translation>Bottom Right</translation>
    </message>
    <message>
        <source>Open Chat</source>
        <translation>Open Chat</translation>
    </message>
    <message>
        <source>Delete Hint</source>
        <translation>Delete Hint</translation>
    </message>
    <message>
        <source>Delete All Hints</source>
        <translation>Delete All Hints</translation>
    </message>
    <message>
        <source>&lt;b&gt;Here&lt;/b&gt; you can see the preview</source>
        <translation>&lt;b&gt;Here&lt;/b&gt; you can see the preview</translation>
    </message>
    <message>
        <source>Show buttons only if notification requires user&apos;s action</source>
        <translation>Show buttons only if notification requires user&apos;s action</translation>
    </message>
    <message>
        <source>Hints size and position...</source>
        <translation>Hints size and position...</translation>
    </message>
</context>
<context>
    <name>HintManager</name>
    <message>
        <source>&lt;table&gt;&lt;tr&gt;&lt;td align=&quot;left&quot; valign=&quot;top&quot;&gt;&lt;img style=&quot;max-width:64px; max-height:64px;&quot; src=&quot;{#{avatarPath} #{avatarPath}}{~#{avatarPath} @{kadu_icons/kadu:64x64}}&quot;&gt;&lt;/td&gt;&lt;td width=&quot;100%&quot;&gt;&lt;div&gt;[&lt;b&gt;%a&lt;/b&gt;][&amp;nbsp;&lt;b&gt;(%g)&lt;/b&gt;]&lt;/div&gt;[&lt;div&gt;&lt;img height=&quot;16&quot; width=&quot;16&quot; src=&quot;#{statusIconPath}&quot;&gt;&amp;nbsp;&amp;nbsp;%u&lt;/div&gt;][&lt;div&gt;&lt;img height=&quot;16&quot; width=&quot;16&quot; src=&quot;@{phone:16x16}&quot;&gt;&amp;nbsp;&amp;nbsp;%m&lt;/div&gt;][&lt;div&gt;&lt;img height=&quot;16&quot; width=&quot;16&quot; src=&quot;@{mail-message-new:16x16}&quot;&gt;&amp;nbsp;&amp;nbsp;%e&lt;/div&gt;]&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;[&lt;hr&gt;&lt;b&gt;%s&lt;/b&gt;][&lt;b&gt;:&lt;/b&gt;&lt;br&gt;&lt;small&gt;%d&lt;/small&gt;]</source>
        <translation>&lt;table&gt;&lt;tr&gt;&lt;td align=&quot;left&quot; valign=&quot;top&quot;&gt;&lt;img style=&quot;max-width:64px; max-height:64px;&quot; src=&quot;{#{avatarPath} #{avatarPath}}{~#{avatarPath} @{kadu_icons/kadu:64x64}}&quot;&gt;&lt;/td&gt;&lt;td width=&quot;100%&quot;&gt;&lt;div&gt;[&lt;b&gt;%a&lt;/b&gt;][&amp;nbsp;&lt;b&gt;(%g)&lt;/b&gt;]&lt;/div&gt;[&lt;div&gt;&lt;img height=&quot;16&quot; width=&quot;16&quot; src=&quot;#{statusIconPath}&quot;&gt;&amp;nbsp;&amp;nbsp;%u&lt;/div&gt;][&lt;div&gt;&lt;img height=&quot;16&quot; width=&quot;16&quot; src=&quot;@{phone:16x16}&quot;&gt;&amp;nbsp;&amp;nbsp;%m&lt;/div&gt;][&lt;div&gt;&lt;img height=&quot;16&quot; width=&quot;16&quot; src=&quot;@{mail-message-new:16x16}&quot;&gt;&amp;nbsp;&amp;nbsp;%e&lt;/div&gt;]&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;[&lt;hr&gt;&lt;b&gt;%s&lt;/b&gt;][&lt;b&gt;:&lt;/b&gt;&lt;br&gt;&lt;small&gt;%d&lt;/small&gt;]</translation>
    </message>
</context>
<context>
    <name>HintOverUserConfigurationWindow</name>
    <message>
        <source>Hint Over Buddy Configuration</source>
        <translation>Hint Over Buddy Configuration</translation>
    </message>
    <message>
        <source>Update preview</source>
        <translation>Update preview</translation>
    </message>
    <message>
        <source>Syntax</source>
        <translation>Syntax</translation>
    </message>
</context>
<context>
    <name>HintsConfigurationUiHandler</name>
    <message>
        <source>Configure</source>
        <translation>Configure</translation>
    </message>
    <message>
        <source>Hint over buddy list: </source>
        <translation>Hint over buddy list: </translation>
    </message>
    <message>
        <source>Advanced hints&apos; configuration</source>
        <translation>Advanced hints&apos; configuration</translation>
    </message>
</context>
<context>
    <name>HintsConfigurationWidget</name>
    <message>
        <source>Configure</source>
        <translation>Configure</translation>
    </message>
    <message>
        <source>&lt;b&gt;Here&lt;/b&gt; you can see the preview</source>
        <translation>&lt;b&gt;Here&lt;/b&gt; you can see the preview</translation>
    </message>
</context>
<context>
    <name>HintsConfigurationWindow</name>
    <message>
        <source>Hints configuration</source>
        <translation>Hints configuration</translation>
    </message>
    <message>
        <source>Don&apos;t hide</source>
        <translation>Don&apos;t hide</translation>
    </message>
    <message>
        <source>&lt;b&gt;Here&lt;/b&gt; you can see the preview</source>
        <translation>&lt;b&gt;Here&lt;/b&gt; you can see the preview</translation>
    </message>
</context>
</TS>
