<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="el">
<context>
    <name>@default</name>
    <message>
        <source>Chat</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Misspelled Words Marking Options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Bold</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Italic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Underline</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Spell Checker Options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Ignore case</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Spelling Suggestions Options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Show suggestions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Maximum number of suggested words</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Spelling</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SpellChecker</name>
    <message>
        <source>Kadu</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Could not find dictionary for %1 language.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Move to &apos;Checked&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Available languages</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Move to &apos;Available languages&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Checked</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Details</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
