<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="pt_BR">
<context>
    <name>@default</name>
    <message>
        <source>Chat</source>
        <translation>Conversa</translation>
    </message>
    <message>
        <source>Antistring</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>General</source>
        <translation>Geral</translation>
    </message>
    <message>
        <source>Enable Antistring</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Block message</source>
        <translation>Bloquear mensagem</translation>
    </message>
    <message>
        <source>Admonition</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Log</source>
        <translation>Log</translation>
    </message>
    <message>
        <source>Write log to file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Conditions</source>
        <translation>Condições</translation>
    </message>
    <message>
        <source>Antistring notifications</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Antistring</name>
    <message>
        <source>     DATA AND TIME      ::   ID   ::    MESSAGE
</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AntistringConfigurationUiHandler</name>
    <message>
        <source>Condition</source>
        <translation>Condição</translation>
    </message>
    <message>
        <source>Don&apos;t use</source>
        <translation>Não use</translation>
    </message>
    <message>
        <source>Factor</source>
        <translation>Fator</translation>
    </message>
    <message>
        <source>Add</source>
        <translation>Adicionar</translation>
    </message>
    <message>
        <source>Change</source>
        <translation>Mudar</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation>Apagar</translation>
    </message>
</context>
<context>
    <name>AntistringNotification</name>
    <message>
        <source>Antistring</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Your interlocutor send you love letter</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
