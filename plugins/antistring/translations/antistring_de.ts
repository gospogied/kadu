<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de">
<context>
    <name>@default</name>
    <message>
        <source>Chat</source>
        <translation>Chat</translation>
    </message>
    <message>
        <source>Antistring</source>
        <translation>Anti-Schneeball-System</translation>
    </message>
    <message>
        <source>General</source>
        <translation>Allgemeines</translation>
    </message>
    <message>
        <source>Enable Antistring</source>
        <translation>Anti-Schneeball-System einschalten</translation>
    </message>
    <message>
        <source>Block message</source>
        <translation>Nachricht blockieren</translation>
    </message>
    <message>
        <source>Admonition</source>
        <translation>Ermahnung</translation>
    </message>
    <message>
        <source>Log</source>
        <translation>Protokoll</translation>
    </message>
    <message>
        <source>Write log to file</source>
        <translation>Protokoll in Datei speichern</translation>
    </message>
    <message>
        <source>Conditions</source>
        <translation>Bedingungen</translation>
    </message>
    <message>
        <source>Antistring notifications</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Antistring</name>
    <message>
        <source>     DATA AND TIME      ::   ID   ::    MESSAGE
</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AntistringConfigurationUiHandler</name>
    <message>
        <source>Condition</source>
        <translation>Bedingung</translation>
    </message>
    <message>
        <source>Don&apos;t use</source>
        <translation>Nicht benutzen</translation>
    </message>
    <message>
        <source>Factor</source>
        <translation>Faktor</translation>
    </message>
    <message>
        <source>Add</source>
        <translation>Hinzufügen</translation>
    </message>
    <message>
        <source>Change</source>
        <translation>Änderung</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation>Löschen</translation>
    </message>
</context>
<context>
    <name>AntistringNotification</name>
    <message>
        <source>Antistring</source>
        <translation type="unfinished">Anti-Schneeball-System</translation>
    </message>
    <message>
        <source>Your interlocutor send you love letter</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
