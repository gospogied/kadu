<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="es">
<context>
    <name>@default</name>
    <message>
        <source>Chat</source>
        <translation>Conversar</translation>
    </message>
    <message>
        <source>Antistring</source>
        <translation>Antistring</translation>
    </message>
    <message>
        <source>General</source>
        <translation>General</translation>
    </message>
    <message>
        <source>Enable Antistring</source>
        <translation>Habilitar Antistring</translation>
    </message>
    <message>
        <source>Block message</source>
        <translation>Bloquear mensaje</translation>
    </message>
    <message>
        <source>Admonition</source>
        <translation>Advertencia</translation>
    </message>
    <message>
        <source>Log</source>
        <translation>Registro</translation>
    </message>
    <message>
        <source>Write log to file</source>
        <translation>Escribir registro en archivo</translation>
    </message>
    <message>
        <source>Conditions</source>
        <translation>Condiciones</translation>
    </message>
    <message>
        <source>Antistring notifications</source>
        <translation>Notificaciones Antistring</translation>
    </message>
</context>
<context>
    <name>Antistring</name>
    <message>
        <source>     DATA AND TIME      ::   ID   ::    MESSAGE
</source>
        <translation>FECHA Y HORA :: ID :: MENSAJE
</translation>
    </message>
</context>
<context>
    <name>AntistringConfigurationUiHandler</name>
    <message>
        <source>Condition</source>
        <translation>Condición</translation>
    </message>
    <message>
        <source>Don&apos;t use</source>
        <translation>No usar</translation>
    </message>
    <message>
        <source>Factor</source>
        <translation>Factor</translation>
    </message>
    <message>
        <source>Add</source>
        <translation>Añadir</translation>
    </message>
    <message>
        <source>Change</source>
        <translation>Cambiar</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation>Eliminar</translation>
    </message>
</context>
<context>
    <name>AntistringNotification</name>
    <message>
        <source>Antistring</source>
        <translation>Antistring</translation>
    </message>
    <message>
        <source>Your interlocutor send you love letter</source>
        <translation>Tu interlocutor te manda carta de amor</translation>
    </message>
</context>
</TS>
