<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="es">
<context>
    <name>@default</name>
    <message>
        <source>Notifications</source>
        <translation>Notificaciones</translation>
    </message>
    <message>
        <source>Sound</source>
        <translation>Sonido</translation>
    </message>
    <message>
        <source>Sound player</source>
        <translation>Reproductor de sonido</translation>
    </message>
    <message>
        <source>Player</source>
        <translation>Reproductor</translation>
    </message>
</context>
</TS>
