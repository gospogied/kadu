<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr">
<context>
    <name>@default</name>
    <message>
        <source>MediaPlayer</source>
        <translation>MediaPlayer</translation>
    </message>
    <message>
        <source>General</source>
        <translation>Général</translation>
    </message>
    <message>
        <source>Media Player Daemon Settings</source>
        <translation>Paramètres du démon Media Player</translation>
    </message>
    <message>
        <source>Host</source>
        <translation>Hôte</translation>
    </message>
    <message>
        <source>Media Player Daemon server address</source>
        <translation>Adresse du serveur du démon Media Player</translation>
    </message>
    <message>
        <source>Port</source>
        <translation>Port</translation>
    </message>
    <message>
        <source>Port number MPD is listening on</source>
        <translation>Le numéro de port MPD écouté sur</translation>
    </message>
    <message>
        <source>Connection timeout (seconds)</source>
        <translation>Connexion échouée après (secondes)</translation>
    </message>
    <message>
        <source>Number of seconds before the connection is terminated</source>
        <translation>Nombre de secondes avant que la connexion soit terminée</translation>
    </message>
</context>
<context>
    <name>MPDMediaPlayer</name>
    <message>
        <source>Unknown</source>
        <translation>Inconnu</translation>
    </message>
</context>
</TS>
