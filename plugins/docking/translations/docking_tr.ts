<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="tr">
<context>
    <name>@default</name>
    <message>
        <source>Tray</source>
        <translation>Sistem tepsisi</translation>
    </message>
    <message>
        <source>General</source>
        <translation>Genel</translation>
    </message>
    <message>
        <source>Blinking Envelope</source>
        <translation>Yanıp Sönen Zarf</translation>
    </message>
    <message>
        <source>Static Envelope</source>
        <translation>Sabit Zarf</translation>
    </message>
    <message>
        <source>Animated Envelope</source>
        <translation>Animasyonlu Zarf</translation>
    </message>
    <message>
        <source>Startup</source>
        <translation>Açılış</translation>
    </message>
    <message>
        <source>Start minimized</source>
        <translation>Simge durumda başlat</translation>
    </message>
    <message>
        <source>Show tooltip over tray icon</source>
        <translation>Sistem tepsisi üzerinde ipucu göster</translation>
    </message>
    <message>
        <source>Tray icon indicating new message</source>
        <translation>Yeni mesajı gösteren sistem tepsisi simgesi</translation>
    </message>
</context>
<context>
    <name>DockingManager</name>
    <message>
        <source>&amp;Restore</source>
        <translation>&amp;Önceki Boyut</translation>
    </message>
    <message>
        <source>&amp;Minimize</source>
        <translation>&amp;Simge Durumuna</translation>
    </message>
    <message>
        <source>&amp;Exit Kadu</source>
        <translation>Kadu&apos;dan &amp;Çık</translation>
    </message>
    <message>
        <source>Status</source>
        <translation>Durum</translation>
    </message>
    <message>
        <source>Description</source>
        <translation>Tanımlama</translation>
    </message>
    <message>
        <source>Statuses</source>
        <translation>Durumlar</translation>
    </message>
    <message>
        <source>Descriptions</source>
        <translation>Tanımlamalar</translation>
    </message>
    <message>
        <source>Silent mode</source>
        <translation>Sessiz mod</translation>
    </message>
</context>
</TS>
