<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en">
<context>
    <name>@default</name>
    <message>
        <source>Tray</source>
        <translation>Tray</translation>
    </message>
    <message>
        <source>General</source>
        <translation>General</translation>
    </message>
    <message>
        <source>Blinking Envelope</source>
        <translation>Blinking Envelope</translation>
    </message>
    <message>
        <source>Static Envelope</source>
        <translation>Static Envelope</translation>
    </message>
    <message>
        <source>Animated Envelope</source>
        <translation>Animated Envelope</translation>
    </message>
    <message>
        <source>Startup</source>
        <translation>Startup</translation>
    </message>
    <message>
        <source>Start minimized</source>
        <translation>Start minimized</translation>
    </message>
    <message>
        <source>Show tooltip over tray icon</source>
        <translation>Show tooltip over tray icon</translation>
    </message>
    <message>
        <source>Tray icon indicating new message</source>
        <translation>Tray icon indicating new message</translation>
    </message>
</context>
<context>
    <name>DockingManager</name>
    <message>
        <source>&amp;Restore</source>
        <translation>&amp;Restore</translation>
    </message>
    <message>
        <source>&amp;Minimize</source>
        <translation>&amp;Minimize</translation>
    </message>
    <message>
        <source>&amp;Exit Kadu</source>
        <translation>&amp;Exit Kadu</translation>
    </message>
    <message>
        <source>Status</source>
        <translation>Status</translation>
    </message>
    <message>
        <source>Description</source>
        <translation>Description</translation>
    </message>
    <message>
        <source>Statuses</source>
        <translation>Statuses</translation>
    </message>
    <message>
        <source>Descriptions</source>
        <translation>Descriptions</translation>
    </message>
    <message>
        <source>Silent mode</source>
        <translation>Silent mode</translation>
    </message>
</context>
</TS>
