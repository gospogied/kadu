<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="es">
<context>
    <name>@default</name>
    <message>
        <source>Tray</source>
        <translation>Bandeja</translation>
    </message>
    <message>
        <source>General</source>
        <translation>General</translation>
    </message>
    <message>
        <source>Blinking Envelope</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Static Envelope</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Animated Envelope</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Startup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Start minimized</source>
        <translation>Iniciar minimizado</translation>
    </message>
    <message>
        <source>Show tooltip over tray icon</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Tray icon indicating new message</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DockingManager</name>
    <message>
        <source>&amp;Restore</source>
        <translation>&amp;Restaurar</translation>
    </message>
    <message>
        <source>&amp;Minimize</source>
        <translation>&amp;Minimizar</translation>
    </message>
    <message>
        <source>&amp;Exit Kadu</source>
        <translation>&amp;Salir de Kadu</translation>
    </message>
    <message>
        <source>Status</source>
        <translation>Estado</translation>
    </message>
    <message>
        <source>Description</source>
        <translation>Descripción</translation>
    </message>
    <message>
        <source>Statuses</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Descriptions</source>
        <translation>Descripciones</translation>
    </message>
    <message>
        <source>Silent mode</source>
        <translation>Modo silencioso</translation>
    </message>
</context>
</TS>
