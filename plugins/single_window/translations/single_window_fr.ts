<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr">
<context>
    <name>@default</name>
    <message>
        <source>Look</source>
        <translation>Apparence</translation>
    </message>
    <message>
        <source>SingleWindow</source>
        <translation> Fenêtre unique</translation>
    </message>
    <message>
        <source>General</source>
        <translation>Général</translation>
    </message>
    <message>
        <source>Roster position</source>
        <translation>Position de Roster</translation>
    </message>
    <message>
        <source>Choose position of roster in Single Window mode</source>
        <translation>Choisir la position de roster en mode Fenêtre unique</translation>
    </message>
    <message>
        <source>Left</source>
        <translation>Gauche</translation>
    </message>
    <message>
        <source>Right</source>
        <translation>Droite</translation>
    </message>
    <message>
        <source>Shortcuts</source>
        <translation>Raccourcis</translation>
    </message>
    <message>
        <source>Switch to previous tab</source>
        <translation>Aller à l&apos;onglet précédent</translation>
    </message>
    <message>
        <source>Switch to next tab</source>
        <translation>Aller à l&apos;onglet suivant</translation>
    </message>
    <message>
        <source>Show/hide roster</source>
        <translation>Afficher / masquer roster</translation>
    </message>
    <message>
        <source>Switch focus between roster and tabs</source>
        <translation>Basculer le focus entre roster et les onglets</translation>
    </message>
    <message>
        <source>Behavior</source>
        <translation>Comportement</translation>
    </message>
</context>
</TS>
