<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="it">
<context>
    <name>@default</name>
    <message>
        <source>Status</source>
        <translation>Stato</translation>
    </message>
    <message>
        <source>Autostatus file</source>
        <translation>Autostatus File</translation>
    </message>
    <message>
        <source>Time</source>
        <translation>Tempo</translation>
    </message>
    <message numerus="yes">
        <source>%n second(s)</source>
        <translation>
            <numerusform>%n secondo</numerusform>
            <numerusform>%n secondi</numerusform>
        </translation>
    </message>
    <message>
        <source>Online</source>
        <translation>In linea</translation>
    </message>
    <message>
        <source>Busy</source>
        <translation>Occupato</translation>
    </message>
    <message>
        <source>Invisible</source>
        <translation>Invisibile</translation>
    </message>
    <message>
        <source>Behavior</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Description</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>AutoStatus</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AutostatusActions</name>
    <message>
        <source>&amp;Autostatus</source>
        <translation>&amp;Autostatus</translation>
    </message>
</context>
</TS>
