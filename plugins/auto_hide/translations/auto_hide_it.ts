<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="it">
<context>
    <name>@default</name>
    <message>
        <source>Autohide idle time</source>
        <translation>Tempo di inattività per l&apos;autonascondi</translation>
    </message>
    <message numerus="yes">
        <source>%n second(s)</source>
        <translation>
            <numerusform>%n secondo</numerusform>
            <numerusform>%n secondi</numerusform>
        </translation>
    </message>
    <message>
        <source>Buddies list</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>General</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Buddies window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Hide buddy list window when unused</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AutoHide</name>
    <message>
        <source>Don&apos;t hide</source>
        <translation>Non nascondere</translation>
    </message>
</context>
</TS>
