<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="es">
<context>
    <name>@default</name>
    <message>
        <source>Chat</source>
        <translation>Conversar</translation>
    </message>
    <message>
        <source>Cenzor</source>
        <translation>Cenzor</translation>
    </message>
    <message>
        <source>General</source>
        <translation>General</translation>
    </message>
    <message>
        <source>Enable cenzor</source>
        <translation>Habilitar Cenzor</translation>
    </message>
    <message>
        <source>Admonition</source>
        <translation>Advertencia</translation>
    </message>
    <message>
        <source>Swearwords</source>
        <translation>Palabrotas</translation>
    </message>
    <message>
        <source>Exclusions</source>
        <translation>Exclusiones</translation>
    </message>
    <message>
        <source>Message was cenzored</source>
        <translation>El mensaje fue censurado</translation>
    </message>
</context>
<context>
    <name>CenzorNotification</name>
    <message>
        <source>Message was cenzored</source>
        <translation>El mensaje fue censurado</translation>
    </message>
    <message>
        <source>Your interlocutor used obscene word and became admonished</source>
        <translation>Su interlocutor usó una palabra obscena y fue amonestado</translation>
    </message>
    <message>
        <source>Cenzor</source>
        <translation>Cenzor</translation>
    </message>
</context>
<context>
    <name>ListEditWidget</name>
    <message>
        <source>Add</source>
        <translation>Añadir</translation>
    </message>
    <message>
        <source>Change</source>
        <translation>Cambiar</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation>Eliminar</translation>
    </message>
</context>
</TS>
