<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="tr">
<context>
    <name>@default</name>
    <message>
        <source>Chat</source>
        <translation>Konuşma</translation>
    </message>
    <message>
        <source>Cenzor</source>
        <translation>Sansür</translation>
    </message>
    <message>
        <source>General</source>
        <translation>General</translation>
    </message>
    <message>
        <source>Enable cenzor</source>
        <translation>Sansürü aç</translation>
    </message>
    <message>
        <source>Admonition</source>
        <translation>Uyarı</translation>
    </message>
    <message>
        <source>Swearwords</source>
        <translation>Kötü Sözler</translation>
    </message>
    <message>
        <source>Exclusions</source>
        <translation>İstisnalar</translation>
    </message>
    <message>
        <source>Message was cenzored</source>
        <translation>Mesaj sansürlendi</translation>
    </message>
</context>
<context>
    <name>CenzorNotification</name>
    <message>
        <source>Message was cenzored</source>
        <translation>Mesaj sansürlendi</translation>
    </message>
    <message>
        <source>Your interlocutor used obscene word and became admonished</source>
        <translation>Konuştuğunuz kişi kötü söz söylediği için uyarıldı</translation>
    </message>
    <message>
        <source>Cenzor</source>
        <translation>Sansü</translation>
    </message>
</context>
<context>
    <name>ListEditWidget</name>
    <message>
        <source>Add</source>
        <translation>Ekle</translation>
    </message>
    <message>
        <source>Change</source>
        <translation>Değiştir</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation>Sil</translation>
    </message>
</context>
</TS>
